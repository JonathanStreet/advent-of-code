#! /usr/bin/env python3
""" Day 3: Binary Diagnostic """

from aoc_elf import read_input
puzzle_input = read_input("input/03.txt")

def part_1():

    gamma_rate = ''
    epsilon_rate = ''

    for i,_ in enumerate(puzzle_input[0]):

        count_1 = 0
        count_0 = 0

        for count,row in enumerate(puzzle_input):

            if row[i] == '0':
                count_0 += 1
            elif row[i] == '1':
                count_1 += 1

        if count_0 > count_1:
            gamma_rate += '0'
            epsilon_rate += '1'
        else:
            gamma_rate += '1'
            epsilon_rate += '0'

    power_consumption = int(gamma_rate,2) * int(epsilon_rate,2)
    print(power_consumption) 

    return


def find_rating(current_input, position, rating_value):
    new_input = []

    count_1 = 0
    count_0 = 0

    # Iterate over the columns at 'position'
    for count,row in enumerate(current_input):
        if row[position] == '0':
            count_0 += 1
        elif row[position] == '1':
            count_1 += 1
    
    # Count the 0's and 1's to find which is most common
    if count_0 > count_1:
        most_common, least_common = ('0','1')
    else:
        most_common, least_common = ('1','0')

    # The rating value determines if we want to keep most or least common
    if rating_value == 'o':
        keep_value = most_common
    elif rating_value == 'c':
        keep_value = least_common

    # Make a list of only the values we want to keep.
    for item in current_input:
        if item[position] == keep_value:
            new_input.append(item)

    # If the list has more than 1 item in it, keep checking
    # otherwise return the value.
    if len(new_input) > 1:
        position += 1   
        return find_rating(new_input, position, rating_value)
    else:
        return new_input[0]


def part_2():

    oxygen_generator_rating = find_rating(puzzle_input, 0, 'o')
    co2_scrubber_rating = find_rating(puzzle_input, 0, 'c')

    life_support_rating = int(oxygen_generator_rating,2) * int(co2_scrubber_rating,2)
    print(life_support_rating)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
