#! /usr/bin/env python3
""" Day 1: Sonar Sweep """

from aoc_elf import read_input
puzzle_input = read_input("input/01.txt")

def part_1():
    counter = 0
    preVal = max(puzzle_input) + 1
    for i in puzzle_input:
        if i > preVal:
            counter += 1
        preVal = i
    print(counter)
    return

def part_2():
    counter = 0
    preVal = sum(puzzle_input)
    for i in range(len(puzzle_input)-2):
        value = puzzle_input[i]\
            + puzzle_input[i+1]\
            + puzzle_input[i+2]
        if value > preVal:
            counter += 1
        preVal = value
    print(counter)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
