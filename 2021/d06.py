#! /usr/bin/env python3
""" Day 6: Lanternfish """

from collections import deque
from aoc_elf import read_input
puzzle_input = list(map(int,read_input("input/06.txt")[0].split(',')))

def solve(days):

    fish = puzzle_input.copy()

    # Initialise the fish into counters
    stats = deque([0]*9)
    for f in fish:
        stats[f] += 1

    # Iterate over the days, keeping stats on the fish internal timers
    for _ in range(days+1):
        stats[6] += stats[8]
        stats.rotate(-1)

    return sum(list(stats))

def part_1():
    print(solve(80))
    return

def part_2():
    print(solve(256))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
