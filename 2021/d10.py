#! /usr/bin/env python3
""" Day 10: Syntax Scoring """

from aoc_elf import read_input
puzzle_input = read_input("input/10.txt")
#puzzle_input = read_input("input/10.ex.txt")

def part_1():

    matches = {
            ')':'(',
            ']':'[',
            '}':'{',
            '>':'<'
            }

    scores = {
            ')' : 3,
            ']' : 57,
            '}' : 1197,
            '>' : 25137
            }

    illegal_chars = []
    for line in puzzle_input:
        chunks = []
        for c in line:
            if c in matches.values():
                chunks.append(c)
            elif matches.get(c) != chunks.pop():
                illegal_chars.append(c)
                break

    error_score = 0
    for i in illegal_chars:
        error_score += scores[i]
    print(error_score)

    return

def part_2():

    matches = {
            ')':'(',
            ']':'[',
            '}':'{',
            '>':'<'
            }

    rev_matches = {
            '(':')',
            '[':']',
            '{':'}',
            '<':'>'
            }

    scores = {
            ')' : 1,
            ']' : 2,
            '}' : 3,
            '>' : 4
            }

    endings = []
    for line in puzzle_input:
        illegal_line = False
        chunks = []
        end_chunks = []

        for c in line:
            if c in matches.values():
                chunks.append(c)
            elif matches.get(c) != chunks.pop():
                illegal_line = True
                break

        if not illegal_line:
            # The chunks we have remaining are the unmatched starting chars
            # Reverse the mappings to get the required endings
            chunks.reverse()
            for a in chunks:
                end_chunks.append(rev_matches[a])
            endings.append(''.join(end_chunks))

    # Scoring calculations
    completion_results = []
    for c in endings:
        total_score = 0
        for i in c:
            total_score *= 5
            total_score += scores[i]
        completion_results.append(total_score)

    # Find the middle score
    print(sorted(completion_results)[len(completion_results)//2])

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
