#! /usr/bin/env python3
""" Day 14: Extended Polymerization """

from aoc_elf import read_input
from collections import Counter

puzzle_input = read_input("input/14.txt","\n\n")
puzzle_input = read_input("input/14.ex.txt","\n\n")

def solve(steps):

    # Parse the input
    polymer_template = puzzle_input[0]
    rules = puzzle_input[1].split("\n")
    mapping = {}
    for r in rules:
        a,b = r.split(" -> ")
        mapping[a] = b

    for step in range(steps):
        new_polymer = ""
        for counter in range(len(polymer_template)-1):
            pair = polymer_template[counter]+polymer_template[counter+1]
            if counter == 0:
                new_polymer += pair[0]
            if pair in mapping:
                new_polymer += mapping[pair]+pair[1]
        polymer_template = new_polymer
        #print(f"After step {step+1}: {new_polymer}")

    counter = Counter(polymer_template)
    most_common = int(counter.most_common()[0][1])
    least_common = int(counter.most_common()[-1:][0][1])
    return most_common - least_common

def part_1():
    print(solve(10))
    return

def part_2():
    # Too slow!
    #print(solve(40))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
