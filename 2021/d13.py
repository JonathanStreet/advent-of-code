#! /usr/bin/env python3
""" Day 13: Transparent Origami """

from aoc_elf import read_input
puzzle_input = read_input("input/13.txt","\n\n")
#puzzle_input = read_input("input/13.ex.txt","\n\n")

def display_plot(coords):

    # Calculate the size of the grid based on co-ordinates
    max_x = max(x[0] for x in coords) + 1
    max_y = max(y[1] for y in coords) + 1

    # Initialise the empty grid
    grid = [[" " for x in range(max_x)] for y in range(max_y)]

    # Plot the co-ordinates
    for x,y in coords:
        grid[y][x] = "#"

    # Print the grid output
    for row in range(max_y):
        print(''.join(grid[row]))

    return

def fold_paper(coords, position, horizontal):

    new_coords = []

    if horizontal:
        # Fold horizontally
        for x,y in coords:
            if int(y) >= int(position):
                new_coords.append((int(x),int(y) - (2 * abs(int(position)-int(y)))))
            else:
                new_coords.append((int(x),int(y)))
    else:
        # Fold vertically
        for x,y in coords:
            if int(x) >= int(position):
                new_coords.append((int(x) - (2 * abs(int(position)-int(x))),int(y)))
            else:
                new_coords.append((int(x),int(y)))

    return new_coords

def part_1():

    # Parse the plot
    plot_points = puzzle_input[0].split("\n")
    coords = []
    for p in plot_points:
        x, y = p.split(",")
        coords.append((int(x),int(y)))

    # Parse the origami instructions
    instructions = puzzle_input[1].split("\n")

    # Do some folding...
    for fold in instructions:

        # Parse instructions:
        position = fold.split("=")[1]
        coords = fold_paper(coords,position,"y" in fold)

        # Stop after first fold
        break

    print(len(set(coords)))
    #display_plot(coords)

    return

def part_2():

    # Parse the plot
    plot_points = puzzle_input[0].split("\n")
    coords = []
    for p in plot_points:
        x, y = p.split(",")
        coords.append((int(x),int(y)))

    # Parse the origami instructions
    instructions = puzzle_input[1].split("\n")

    # Do some folding...
    for fold in instructions:

        # Parse instructions:
        position = fold.split("=")[1]
        coords = fold_paper(coords,position,"y" in fold)

    display_plot(coords)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
