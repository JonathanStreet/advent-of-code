#! /usr/bin/env python3
""" Day 4: Giant Squid """

from aoc_elf import read_input
puzzle_input = read_input("input/04.txt")
#puzzle_input = read_input("input/04.ex.txt")

def check_results(board):

    global winning_list

    # Max numbers on a single board
    numbers_on_board = 25

    # The number of boards in play
    number_of_boards = int(len(board) / numbers_on_board)

    # Iterate over the boards in play
    shift = 0

    for i in range(number_of_boards):

        current_board = board[shift:shift+numbers_on_board]
        shift += numbers_on_board

        r_shift = 0
        for j in range(5):

            # Check rows
            row = current_board[r_shift:r_shift+5]

            # Check columns
            column = \
            current_board[j+0] + \
            current_board[j+5] + \
            current_board[j+10] + \
            current_board[j+15] + \
            current_board[j+20]

            # Win condition
            if ((''.join(row) == 'XXXXX') or (column == 'XXXXX')):
                return (i+1,current_board)

            r_shift += 5

    return (0,[])

def check_results_p2(board):

    global winning_list

    # Max numbers on a single board
    numbers_on_board = 25

    # The number of boards in play
    number_of_boards = int(len(board) / numbers_on_board)

    # Iterate over the boards in play
    shift = 0

    for i in range(number_of_boards):

        current_board = board[shift:shift+numbers_on_board]
        shift += numbers_on_board

        r_shift = 0
        for j in range(5):

            # Check rows
            row = current_board[r_shift:r_shift+5]

            # Check columns
            column = \
            current_board[j+0] + \
            current_board[j+5] + \
            current_board[j+10] + \
            current_board[j+15] + \
            current_board[j+20]

            # Win condition
            if ((''.join(row) == 'XXXXX') or (column == 'XXXXX')) \
                    and (i+1 not in winning_list):
                winning_list.append(i+1)
                if len(winning_list) == number_of_boards:
                    return (i+1,current_board)

            r_shift += 5

    return (0,[])


def part_1():

    # The bingo calls
    calls = puzzle_input[0].split(',')

    # All the boards
    boards = puzzle_input[1:]

    # Max numbers on a single board
    numbers_on_board = 25

    # The number of boards in play
    number_of_boards = int(len(boards) / numbers_on_board)

    results = []
    results = boards
    winning_board_number = 0
    winning_board = []
    global winning_list
    winning_list = []

    for i in calls:

        # Substitute i with 'X' in the list.
        results = ['X' if x==i else x for x in results]

        # Check if any board has won
        winning_board_number, winning_board = check_results(results)
        
        if (winning_board_number > 0):
            result = 0

            # Calculate the remaining numbers
            remaining_numbers = [j for j in winning_board if 'X' not in j]
            for x in remaining_numbers:
                result = result + int(x)

            # Display the results
            print(int(result) * int(i))
            break

    return

def part_2():

    # The bingo calls
    calls = puzzle_input[0].split(',')

    # All the boards
    boards = puzzle_input[1:]

    # Max numbers on a single board
    numbers_on_board = 25

    # The number of boards in play
    number_of_boards = int(len(boards) / numbers_on_board)

    results = []
    results = boards
    winning_board_number = 0
    winning_board = []
    global winning_list
    winning_list = []

    for i in calls:

        # Substitute i with 'X' in the list.
        results = ['X' if x==i else x for x in results]

        # Check if any board has won
        winning_board_number, winning_board = check_results_p2(results)

        if len(winning_list) == number_of_boards:
            result = 0

            # Calculate the remaining numbers
            remaining_numbers = [j for j in winning_board if 'X' not in j]
            for x in remaining_numbers:
                result = result + int(x)

            # Display the results
            print(int(result) * int(i))
            break

    return


print("Part 1:")
part_1()

print("Part 2:")
part_2()

