#! /usr/bin/env python3
""" Day 7: The Treachery of Whales """

from aoc_elf import read_input
puzzle_input = list(map(int,read_input("input/07.txt")[0].split(',')))
#puzzle_input = list(map(int,read_input("input/07.ex.txt")[0].split(',')))

def calculate_fuel_cost(position, part1):

    cost = 0 
    for j in puzzle_input:
        if part1:
            cost += abs(j - position)
        else:
            temp_cost = abs(j - position)
            cost += int(abs(temp_cost * (temp_cost+1)/2))
    return cost

def part_1():

    cost = []

    min_fuel = min(puzzle_input)
    max_fuel = max(puzzle_input)

    # Brute force over all possible move positions
    for i in range(min_fuel,max_fuel):
        cost.append(calculate_fuel_cost(i,True))

    print(min(cost))
    return

def part_2():

    cost = []

    min_fuel = min(puzzle_input)
    max_fuel = max(puzzle_input)

    # Brute force over all possible move positions
    for i in range(min_fuel,max_fuel):
        cost.append(calculate_fuel_cost(i,False))

    print(min(cost))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
