#! /usr/bin/env python3
""" Day 5: Hydrothermal Venture """

from aoc_elf import read_input
import re

puzzle_input = read_input("input/05.txt","\n")
#puzzle_input = read_input("input/05.ex.txt","\n")

def calculate(diagonal_check):

    regex = re.compile(r'([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)')

    vents = []

    # Set the size of the grid
    max_x = 1000
    max_y = 1000

    for line in puzzle_input:

        # Parse the input
        x1,y1,x2,y2 = regex.findall(line)[0]

        # Check diagonals or only horizontal and vertical lines
        if diagonal_check:
            vents.append((int(x1),int(y1),int(x2),int(y2)))
        elif ((x1 == x2) or (y1 == y2)):
            vents.append((int(x1),int(y1),int(x2),int(y2)))

    # Initialise empty grid
    grid = [[0 for x in range(max_x)] for y in range(max_y)]

    # Plot the vents onto the grid
    for x1,y1,x2,y2 in vents:

        # Plot horizontal
        if (x1 == x2):
            for y in range(min(y1,y2),max(y1,y2)+1):
                grid[x1][y] += 1

        # Plot vertical
        elif (y1 == y2):
            for x in range(min(x1,x2),max(x1,x2)+1):
                grid[x][y1] += 1

        # Plot diagonal
        else:
            x, y = x1, y1
            if (x1 < x2) and (y1 < y2):
                while x <= x2 and y <= y2:
                    grid[x][y] += 1
                    x += 1
                    y += 1

            elif (x1 > x2) and (y1 < y2):
                while x >= x2 and y <= y2:
                    grid[x][y] += 1
                    x -= 1
                    y += 1

            elif (x1 < x2) and (y1 > y2):
                while x <= x2 and y >= y2:
                    grid[x][y] += 1
                    x += 1
                    y -= 1

            elif (x1 > x2) and (y1 > y2):
                while x >= x2 and y >= y2:
                    grid[x][y] += 1
                    x -= 1
                    y -= 1

    # Count the number of overlaps
    counter = 0
    for x in range(max_x):
        for y in range(max_y):
            if grid[x][y] > 1:
                counter += 1

    return counter

def part_1():
    print(calculate(False))
    return

def part_2():
    print(calculate(True))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

