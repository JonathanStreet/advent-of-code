#! /usr/bin/env python3
""" Day 11: Dumbo Octopus """

from aoc_elf import read_input
import textwrap
puzzle_input = read_input("input/11.txt")

# Put puzzle_input into a grid.
grid = {}
row_counter = len(puzzle_input)
col_counter = len(str(puzzle_input[0]))
for row, line in enumerate(puzzle_input):
    for col, item in enumerate(str(line)):
        grid[(col,row)] = int(item)

flashed = []

def try_flash(octopus_state, x, y):

    global flashed

    if (x < 0) or (y < 0): return octopus_state
    if (x >= col_counter) or (y >= row_counter): return octopus_state

    if (octopus_state[(x,y)] > 9) and (x,y) not in flashed:
        flashed.append((x,y))

        # Flash surrounding values
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                try:
                    octopus_state[(x+i,y+j)] += 1
                except:
                    ''
                octopus_state = try_flash(octopus_state,x+i,y+j)

    return octopus_state

def step(octopus_state):

    global flashed
    flashes = 0

    # Increment energy levels
    for x in range(row_counter):
        for y in range(col_counter):
            octopus_state[(x,y)] += 1

    # If energy level of octopus is greater than 9, flash (& check any
    # cascading flashes)
    for x in range(row_counter):
        for y in range(col_counter):
            octopus_state = try_flash(octopus_state, x, y)

    flashes = len(flashed)
    flashed = []

    # Reset any octopus to 0 (& count the flashes)
    for x in range(row_counter):
        for y in range(col_counter):
            if octopus_state[(x,y)] > 9:
                octopus_state[(x,y)] = 0

    octopus_state_after = octopus_state
    return (octopus_state_after, flashes)

def part_1():

    octopus_state = grid.copy()
    flash_counter = 0

    for _ in range(100):
        octopus_state, flashes = step(octopus_state)
        flash_counter += flashes

    print(flash_counter)
    return

def part_2():

    octopus_state = grid.copy()
    counter = 0
    while True:
        octopus_state, flashes = step(octopus_state)
        counter += 1
        if flashes == 100:
            break
    print(counter)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
