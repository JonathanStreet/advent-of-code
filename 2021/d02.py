#! /usr/bin/env python3
""" Day 2: Dive! """

from aoc_elf import read_input
puzzle_input = read_input("input/02.txt","\n")

def part_1():
    
    horizontal = 0
    depth = 0

    for l in puzzle_input:
        instruction, distance = l.split()

        if instruction == 'forward':
            horizontal += int(distance)
        elif instruction == 'down':
            depth += int(distance)
        elif instruction == 'up':
            depth -= int(distance)
    
    print(horizontal*depth)

    return

def part_2():

    horizontal = 0
    depth = 0
    aim = 0

    for l in puzzle_input:
        instruction, distance = l.split()

        if instruction == 'forward':
            horizontal += int(distance)
            depth += aim * int(distance)
        elif instruction == 'down':
            aim += int(distance)
        elif instruction == 'up':
            aim -= int(distance)

    print(horizontal*depth)   

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
