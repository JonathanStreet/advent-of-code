#! /usr/bin/env python3
""" Day 9: Smoke Basin """

from aoc_elf import read_input
puzzle_input = read_input("input/09.txt",None,"str")
#puzzle_input = read_input("input/09.ex.txt",None,"str")

global visited
visited = []

def find_low_points():

    min_x, max_x = 0, len(puzzle_input[0])-1
    min_y, max_y = 0, len(puzzle_input)-1
    risk = 0
    lowest_points = []

    for y,row in enumerate(puzzle_input):
        for x,cell in enumerate(row):

            # Ensure we are within the bounds, and...
            # ...check if value to the left is bigger than cell
            if (x > min_x) and (int(cell) >= int(puzzle_input[y][x-1])):
                continue

            # ...check if value to the right is bigger than cell
            if (x < max_x) and (int(cell) >= int(puzzle_input[y][x+1])):
                continue

            # ...check if value above is bigger than cell
            if (y > min_y) and (int(cell) >= int(puzzle_input[y-1][x])):
                continue

            # ...check if value below is bigger than cell
            if (y < max_y) and (int(cell) >= int(puzzle_input[y+1][x])):
                continue

            lowest_points.append((x,y))

    return lowest_points

def out_of_bounds(point):
    x,y = point
    if (int(x) < 0) or (int(y) < 0):
        return True
    elif (int(x) >= len(puzzle_input[0])) or (int(y) >= len(puzzle_input)):
        return True
    else:
        return False

def find_basin_size(point):

    global visited
    x,y = point

    # Exit if out-of-bounds
    if out_of_bounds(point):
        return len(visited)

    # Exit if already visited
    if point in visited:
        return len(visited)

    # Exit if boundary of basin
    if int(puzzle_input[y][x]) == 9:
        return len(visited)

    # Keep track of the visited points
    visited.append(point)

    # Check surrounding points (up, down, left, right)
    find_basin_size((x-1,y))
    find_basin_size((x+1,y))
    find_basin_size((x,y-1))
    find_basin_size((x,y+1))

    return len(visited)

def part_1():

    risk = 0

    for x,y in find_low_points():
        risk += int(puzzle_input[y][x]) + 1

    print(risk)

    return

def part_2():

    global visited
    basin_sizes = []
    for point in find_low_points():
        visited = []
        basin_sizes.append(find_basin_size(point))

    #print(basin_sizes)

    basin_sizes = sorted(basin_sizes)
    result = basin_sizes[-1] * basin_sizes[-2] * basin_sizes[-3]

    print(result)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
