#! /usr/bin/env python3
""" Day 8: Seven Segment Search """

from aoc_elf import read_input
puzzle_input = read_input("input/08.txt","\n")

def part_1():

    output_value = []
    count = 0
    for line in puzzle_input:
        unique_signal_patterns_str, output_value_str = line.split("|")
        output_value = output_value_str.split()

        # Char 1 = 2 segments
        # Char 4 = 4 segments
        # Char 7 = 3 segments
        # Char 8 = 7 segments
        for i in output_value:
            if (len(i) in [2,4,3,7]):
                count += 1

    print(count)
    return

def decode(encoded_str):

    encoded = encoded_str.split()
    decoded = []
    five_seg = []
    six_seg = []

    # Initialise all numbers, in case they are not in the input:
    one = ""
    two = ""
    three = ""
    four = ""
    five = ""
    six = ""
    seven = ""
    eight = ""
    nine = ""
    zero = ""

    # Deduce the numbers we can by counting the segments
    for pattern in encoded:
        if len(pattern) == 2:
            one = pattern
        elif len(pattern) == 3:
            seven = pattern
        elif len(pattern) == 4:
            four = pattern
        elif len(pattern) == 5:
            five_seg.append(pattern)
        elif len(pattern) == 6:
            six_seg.append(pattern)
        elif len(pattern) == 7:
            eight = pattern

    # Deduce the five-segment numbers
    for pattern in five_seg:
        # The segments in 1 are all found in 3
        if len(set(one).intersection(pattern)) == 2:
            three = pattern
        # 3/4 segments of 4 are found in 5
        elif len(set(four).intersection(pattern)) == 3:
            five = pattern
        # Remaining number must be two
        else:
            two = pattern

    # Deduce the six-segment numbers
    for pattern in six_seg:
        # The segments in 4 are all found in 9
        if len(set(four).intersection(pattern)) == 4:
            nine = pattern
        # The segments in 1 are all found in 0
        elif len(set(one).intersection(pattern)) == 2:
            zero = pattern
        # Remaining number must be six
        else:
            six = pattern

    # Decode the encoded message
    for i in encoded:
        if sorted(i) == sorted(zero):
            decoded.append("0")
        elif sorted(i) == sorted(one):
            decoded.append("1")
        elif sorted(i) == sorted(two):
            decoded.append("2")
        elif sorted(i) == sorted(three):
            decoded.append("3")
        elif sorted(i) == sorted(four):
            decoded.append("4")
        elif sorted(i) == sorted(five):
            decoded.append("5")
        elif sorted(i) == sorted(six):
            decoded.append("6")
        elif sorted(i) == sorted(seven):
            decoded.append("7")
        elif sorted(i) == sorted(eight):
            decoded.append("8")
        elif sorted(i) == sorted(nine):
            decoded.append("9")
        elif i == "|":
            decoded.append("|")

    return ''.join(decoded)

def part_2():

    numbers = []
    count = 0

    for line in puzzle_input:

        decoded_line = decode(line)
        count += int(decoded_line.split('|')[1],10)

    print(count)

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
