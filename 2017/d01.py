#! /usr/bin/env python3
""" Day 1: Inverse Captcha """

# --- Part 1 ---
i = 0
prevChar = ""
firstChar = ""
total = 0
with open("input/01.txt") as fileobj:
    for line in fileobj:
        for ch in line:
            if (i == 0):
                firstChar = ch
                i += 1
            else:
                if (ch == prevChar):
                    total += int(ch)
            prevChar = ch

if (firstChar == line.strip()[-1]):
    total += int(firstChar)

print("Part 1:",total)


# --- Part 2 ---
total = 0

if __name__ == "__main__":
    with open("input/01.txt") as file:
        input = file.read().strip()

# Halfway around the list
offset = int(len(input) / 2)

# Shift the list by the offset
shifted_input = input[offset:] + input[:offset]

# Zip the list together and compare side-by-side values
for c,n in zip(input, shifted_input):
    if c == n:
        total += int(c)

print("Part 2:",total)


