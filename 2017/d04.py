#! /usr/bin/env python3
""" Day 4: High-Entropy Passphrases """

# --- Part 1 ---
count = 0
invalid = 0
with open("input/04.txt") as f:
    for line in f:
        line = line.strip()
        list = [i for i in line.split(' ')]
        count += 1
        if (len(list) != len(set(list))):
           invalid += 1

print(count - invalid)

# --- Part 2 ---
count = 0
invalid = 0
with open("input/04.txt") as f:
    for line in f:
        line = line.strip()
        list = [i for i in line.split(' ')]
        # Sort each array item
        list = [''.join(sorted(i)) for i in list]
        count += 1
        if (len(list) != len(set(list))):
           invalid += 1

print(count - invalid)
