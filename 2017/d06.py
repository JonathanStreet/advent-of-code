#! /usr/bin/env python3
""" Day 6: Memory Reallocation """

# --- Part 1 ---
count = 0
known_config = []
no_duplicates = True

with open("input/06.txt") as f:
    contents = f.read()[:-1]
    banks = [int(i) for i in contents.split()]
#    banks = [0,2,7,0]

    while no_duplicates:
        count += 1
        m = max(banks)
        p = [ i for i,v in enumerate(banks) if v==m ]

        # Save list into 'known configuration' list and 
        # convert into a string so it's easier to compare
        known_config.append(str(banks).strip('[]'))

        # Loop through list until 'm = 0'
        n = p[0]
        banks[p[0]] = 0
        while (m > 0):
            n += 1
            banks[n % len(banks)] = banks[n % len(banks)] + 1
            m -= 1

        # Break outer-loop if config has already been used.
        if ( len(known_config) != len(set(known_config)) ):
            no_duplicates = False

print("Part 1:",count - 1)


# --- Part 2 ---

count = 0
known_config = []
no_duplicates = True

with open("input/06.txt") as f:
    contents = f.read()[:-1]
    banks = [int(i) for i in contents.split()]
#    banks = [0,2,7,0]

    while no_duplicates:
        count += 1
        m = max(banks)
        p = [ i for i,v in enumerate(banks) if v==m ]

        # Save list into 'known configuration' list and 
        # convert into a string so it's easier to compare
        known_config.append(str(banks).strip('[]'))

        # Loop through list until 'm = 0'
        n = p[0]
        banks[p[0]] = 0
        while (m > 0):
            n += 1
            banks[n % len(banks)] = banks[n % len(banks)] + 1
            m -= 1

        # Break outer-loop if config has already been used.
        if ( len(known_config) != len(set(known_config)) ):
            no_duplicates = False

# Duplicate will be the last value in the 'known_config' array.
duplicate = known_config[-1]
#print(duplicate)

# Total list items (minus) first occurance of item...

first = known_config.index(duplicate)
print("Part 2:",len(known_config) - first - 1)

