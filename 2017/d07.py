#! /usr/bin/env python3
""" Day 7: Recursive Circus """

# --- Part 1 ---
output = ""
allPrograms = []

with open("input/07.txt") as f:
    contents = f.read()[:-1]
    lines = [i for i in contents.split("\n")]


    for line in lines:
        children = []
        
        # Retreive name, and weight (stripping characters)
        name = line.split()[0].split(' ')[0]
        weight = line.split(' ')[1].replace('(','').replace(')','')
       
        allPrograms.append(name)

        # Strip spaces, and find children
        line = line.replace(" ","")
        if '->' in line:
            children = line.split('->')[1].split(',')
            for child in children:
                allPrograms.append(child)

        #print "Name:",name
        #print "Weight:",weight
        #print "Children:",children
  
# Remove any duplicates in list, remaining program is the initial node.
d = {}
for i in allPrograms: d[i] = d.__contains__(i)

print([k for k in d.keys() if not d[k]][0])

