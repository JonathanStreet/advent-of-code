#! /usr/bin/env python3
""" Day 19: A Series of Tubes """

# --- Part 1 & 2 ---
with open("input/19.txt") as f:
    contents = f.read()[:-1]
    lines = [i for i in contents.split("\n")]

x = lines[0].index('|')
y = -1
character = lines[y+1][x]
counter = 0
direction = "D"
answer = ""

while character != ' ':
    counter += 1
    if direction == "D":
        y += 1
    elif direction == "U":
        y -= 1
    elif direction == "R":
        x += 1
    elif direction == "L":
        x -= 1
    else:
        print("Error, what's broken?")
        break

    character = lines[y][x]
    if character not in "|-+ ":
        answer += character

    if character == "+":
        if direction in "DU":
            # Turn left
            if lines[y][x-1] != " ":
                direction = "L"
            # Turn right
            if lines[y][x+1] != " ":
                direction = "R"
        elif direction in "RL":
            # Turn Up
            if lines[y-1][x] != " ":
                direction = "U"
            # Turn Down
            if lines[y+1][x] != " ":
                direction = "D"
            

#print lines[y-1][x-1], lines[y-1][x], lines[y-1][x+1]
#print lines[y][x-1],   lines[y][x],   lines[y][x+1]
#print lines[y+1][x-1], lines[y+1][x], lines[y+1][x+1]

print("Part 1:",answer)
print("Part 2:",counter)
