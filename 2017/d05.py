#! /usr/bin/env python3
""" Day 5: A Maze of Twisty Trampolines, All Alike """

# --- Part 1 ---
count = 0
with open("input/05.txt") as f:
    contents = f.read()[:-1]
    lines = contents.split("\n")

    #lines = [0,3,0,1,-3]
    
    list_min = 0
    list_max = len(lines) - 1
    position = 0
    nextPosition = 0

    while True:
        nextPosition += int(lines[position])
        lines[position] = int(lines[position]) + 1
        position = nextPosition
        count += 1
        if ((position < list_min) or (position > list_max)):
            break

print(count)


