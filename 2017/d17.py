#! /usr/bin/env python3
""" Day 17: Spinlock """

# --- Part 1 ---
i = 0
pos = 0
input = 363
spinlock = [0]

while (i <= 2017):
    i += 1
    pos = ((pos + input) % len(spinlock)) + 1
    spinlock.insert(pos,i)

print("Part 1:",spinlock[spinlock.index(2017) + 1])


# --- Part 2 ---

i = 0
pos = 0
input = 363
spinlock = [0]
numValues = 50000000

while (i <= numValues):
    i += 1
    pos = ((pos + input) % i) + 1
    if pos == 1:
        ans = i

print("Part 2:",ans)


