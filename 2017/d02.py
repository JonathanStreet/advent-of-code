#! /usr/bin/env python3
""" Day 2: Corruption Checksum """

# --- Part 1 ---
i = 0
total = 0        
with open("input/02.txt") as fileobj:
    for line in fileobj:
        line = line.strip()
        line = [int(i) for i in line.split(' ')]
        total += (max(line) - min(line))
        #print line
        #print min(line), max(line)
print(total)

# --- Part 2 ---
import itertools

i = 0
total = 0        
with open("input/02.txt") as fileobj:
    for line in fileobj:
        line = line.strip()
        line = [int(i) for i in line.split(' ')]
        #print line

        for i in itertools.combinations(line, 2):
            if max(i) % min(i) == 0:
                total += max(i) / min(i)    
                break

print(int(total))
