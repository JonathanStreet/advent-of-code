#! /usr/bin/env python3
""" Day 6: Probably a Fire Hazard """

from aoc_elf import read_input
import numpy as np
import re

puzzle_input = read_input("input/06.txt","\n")

def part_1():

    grid = np.zeros((1000,1000), dtype=bool)

    regex = re.compile(r'(turn off|turn on|toggle)\s(\d+),(\d+)\sthrough\s(\d+),(\d+).*')

    for p in puzzle_input:
        op, x1, y1, x2, y2 = re.findall(regex, p)[0]

        x1 = int(x1)
        y1 = int(y1)
        x2 = int(x2) + 1
        y2 = int(y2) + 1

        width = x2 - x1
        height = y2 - y1
        mask = np.ones((height,width), dtype=bool)

        match op:
            case "turn on":
                grid[y1:y2,x1:x2] = True
            case "turn off":
                grid[y1:y2,x1:x2] = False
            case "toggle":
                grid[y1:y2,x1:x2] = grid[y1:y2, x1:x2] ^ mask

    print(np.sum(grid))

    return

def part_2():

    grid = np.zeros((1000,1000), dtype=int)

    regex = re.compile(r'(turn off|turn on|toggle)\s(\d+),(\d+)\sthrough\s(\d+),(\d+).*')

    for p in puzzle_input:
        op, x1, y1, x2, y2 = re.findall(regex, p)[0]

        x1 = int(x1)
        y1 = int(y1)
        x2 = int(x2) + 1
        y2 = int(y2) + 1

        match op:
            case "turn on":
                grid[y1:y2,x1:x2] += 1
            case "turn off":
                grid[y1:y2,x1:x2] -= 1
                grid[grid < 0] = 0
            case "toggle":
                grid[y1:y2,x1:x2] += 2

    print(np.sum(grid))

    return

print("Part 1:")
part_1()
print("Part 2:")
part_2()

