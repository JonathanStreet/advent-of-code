#! /usr/bin/env python3
""" Day 4: The Ideal Stocking Stuffer """

from aoc_elf import read_input
import hashlib

puzzle_input = read_input("input/04.txt")[0]

def part_1():

    secret = puzzle_input
    number = 0
    while True:
        number += 1
        hash_output = hashlib.md5((secret+""+str(number)).encode('utf-8')).hexdigest()
        if (hash_output[:5] == "00000"):
            break
    print(number)
    return

def part_2():
    secret = puzzle_input
    number = 0
    while True:
        number += 1
        hash_output = hashlib.md5((secret+""+str(number)).encode('utf-8')).hexdigest()
        if (hash_output[:6] == "000000"):
            break
    print(number)
    return

print("Part 1:")
part_1()
print("Part 2:")
part_2()

