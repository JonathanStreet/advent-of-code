#! /usr/bin/env python3
""" Day 1: Not Quite Lisp """

from aoc_elf import read_input
puzzle_input = read_input("input/01.txt")[0]

def solve(check_position):
    floor = 0;
    charpos = 0;
    for ch in puzzle_input:
        if (floor == -1) and check_position:
            break
        charpos += 1
        if (ch == '('):
            floor += 1
        elif (ch == ')'):
            floor -= 1
    return floor, charpos

def part_1():
    floor, _ = solve(False)
    print(floor)
    return

def part_2():
    _, charpos = solve(True)
    print(charpos)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
