#! /usr/bin/env python3
""" Day 5: Doesn't He Have Intern-Elves For This? """

from aoc_elf import read_input
import re

puzzle_input = read_input("input/05.txt")

def is_nice_string_1(string):

    # Must contain at least three vowels
    if not re.findall(r'(.*[aeiou]){3}', string):
       return False

    # Must contain a least one letter that appears twice in a row
    if not re.findall(r'(.)\1', string):
        return False

    # Must not contain 'ab', 'cd', 'pq' or 'xy'
    if re.findall(r'(ab|cd|pq|xy)',string):
        return False

    return True

def is_nice_string_2(string):

    # Must contain a pair of two letters that appear at least twice in the
    # string without overlapping.
    if not re.findall(r'(..).*\1', string):
       return False

    # Must contain at least one letter which repeats with exactly one letter
    # between them.
    if not re.findall(r'(.).\1', string):
        return False

    return True

def part_1():
    count = 0
    for p in puzzle_input:
        if is_nice_string_1(p):
            count += 1
    print(count)
    return

def part_2():
    count = 0
    for p in puzzle_input:
        if is_nice_string_2(p):
            count += 1
    print(count)
    return

print("Part 1:")
part_1()
print("Part 2:")
part_2()

