#! /usr/bin/env python3
""" Day 3: Perfectly Spherical Houses in a Vacuum """

from aoc_elf import read_input
puzzle_input = read_input("input/03.txt")[0]

def part_1():
    x = 0
    y = 0
    houses_visited = []
    houses_visited.append((x,y))
    for ch in puzzle_input:
        match ch.lower():
            case "^":
                y += 1
            case ">":
                x += 1
            case "v":
                y -= 1
            case "<":
                x -= 1
        houses_visited.append((x,y))
    print(len(set(houses_visited)))
    return

def part_2():

    x = 0
    y = 0
    rx = 0
    ry = 0
    houses_visited = []
    houses_visited.append((x,y))
    houses_visited.append((rx,ry))

    i = 0
    for ch in puzzle_input:
        if (i % 2 == 1):
            match ch.lower():
                case "^":
                    y += 1
                case ">":
                    x += 1
                case "v":
                    y -= 1
                case "<":
                    x -= 1
            houses_visited.append((x,y))
        else:
            match ch.lower():
                case "^":
                    ry += 1
                case ">":
                    rx += 1
                case "v":
                    ry -= 1
                case "<":
                    rx -= 1
            houses_visited.append((rx,ry))
        i += 1
    print(len(set(houses_visited)))
    return

print("Part 1:")
part_1()
print("Part 2:")
part_2()

