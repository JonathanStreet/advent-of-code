#! /usr/bin/env python3
""" Day 8: Matchsticks """

from aoc_elf import read_input

puzzle_input = read_input("input/08.txt","\n")

def literal_char_count(string):
    return len(string)

def memory_char_count(string):
    count = 0
    ch = 0
    while ch < len(string):
        if string[ch] == '"':
            ch += 1
        elif string[ch] == '\\':
            count += 1
            if string[ch+1] == "x":
                ch += 4
            else:
                ch += 2
        else:
            ch += 1
            count += 1
    return count

def encode(string):
    new_string = "\""
    for ch in string:
        if ch == '"':
            new_string += "\\"
        elif ch == "\\":
            new_string += "\\"
        new_string += ch
    new_string += "\""
    return new_string

def part_1():
    literals, memory = 0, 0
    for entry in puzzle_input:
        literals += literal_char_count(entry)
        memory += memory_char_count(entry)
    print(literals - memory)
    return

def part_2():
    literals, encoded = 0, 0
    for entry in puzzle_input:
        literals += literal_char_count(entry)
        encoded += literal_char_count(encode(entry))
    print(encoded - literals)
    return

print("Part 1:")
part_1()
print("Part 2:")
part_2()
