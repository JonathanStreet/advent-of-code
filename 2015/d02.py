#! /usr/bin/env python3
""" Day 2: I Was Told There Would Be No Math """

from aoc_elf import read_input
puzzle_input = read_input("input/02.txt")

def part_1():
    total = 0;
    for line in puzzle_input:
        dimensions = 0

        # Removes '\n' new-lines from the string
        line = line.strip()

        # Splits the line string into an array - typecast to integers
        l, w, h = [int(i) for i in line.split('x')]

        # Find the slackest side:
        slack = min(l*w, w*h, h*l)

        #val = reduce (operator.mul, dimensions)
        val = (2*l*w + 2*w*h + 2*h*l)

        total += val + slack

    print(total)
    return

def part_2():

    total = 0;

    for line in puzzle_input:

        # Removes '\n' new-lines from the string
        line = line.strip()

        # Splits the line string into an array - typecast to integers
        l, w, h = [int(i) for i in line.split('x')]

        bow = (w*l*h)

        ordered = sorted([l,w,h])
        ribbon = (int(ordered[0])*2)+(int(ordered[1])*2)

        total += ribbon + bow

    print(total)
    return

print("Part 1:")
part_1()
print("Part 2:")
part_2()

