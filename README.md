# Advent of Code Solutions

[Advent of Code](https://adventofcode.com) is a series of small programming puzzles. Created by [Eric Wastl](https://github.com/topaz).


## About this repo

This repository contains a list of my solutions for the puzzles I have solved each year.
Currently my solutions are written in Python (which I learnt for this challenge).


## Year by year

### 2022

[https://adventofcode.com/2022/][aoc2022]; 14/50 stars (28% complete).

* `**` Day 1: Calorie Counting
* `**` Day 2: Rock Paper Scissors
* `**` Day 3: Rucksack Reorganization
* `**` Day 4: Camp Cleanup
* `**` Day 5: Supply Stacks
* `**` Day 6: Tuning Trouble
* `**` Day 7: No Space Left On Device
* `__` Day 8: Treetop Tree House
* `__` Day 9: Rope Bridge
* `__` Day 10: Cathode-Ray Tube
* `__` Day 11: Monkey in the Middle
* `__` Day 12: Hill Climbing Algorithm
* `__` Day 13: Distress Signal
* `__` Day 14: Regolith Reservoir
* `__` Day 15: Beacon Exclusion Zone
* `__` Day 16: Proboscidea Volcanium
* `__` Day 17: Pyroclastic Flow
* `__` Day 18: Boiling Boulders
* `__` Day 19: Not Enough Minerals
* `__` Day 20: Grove Positioning System
* `__` Day 21: Monkey Math
* `__` Day 22: Monkey Map
* `__` Day 23: Unstable Diffusion
* `__` Day 24: Blizzard Basin
* `__` Day 25: Full of Hot Air


### 2021

[https://adventofcode.com/2021/][aoc2021]; 25/50 stars (50% complete).

* `**` Day 1: Sonar Sweep
* `**` Day 2: Dive!
* `**` Day 3: Binary Diagnostic
* `**` Day 4: Giant Squid
* `**` Day 5: Hydrothermal Venture
* `**` Day 6: Lanternfish
* `**` Day 7: The Treachery of Whales
* `**` Day 8: Seven Segment Search
* `**` Day 9: Smoke Basin
* `**` Day 10: Syntax Scoring
* `**` Day 11: Dumbo Octopus
* `__` Day 12: Passage Pathing
* `**` Day 13: Transparent Origami
* `*_` Day 14: Extended Polymerization
* `__` Day 15: Chiton
* `__` Day 16: Packet Decoder
* `__` Day 17: Trick Shot
* `__` Day 18: Snailfish
* `__` Day 19: Beacon Scanner
* `__` Day 20: Trench Map
* `__` Day 21: Dirac Dice
* `__` Day 22: Reactor Reboot
* `__` Day 23: Amphipod
* `__` Day 24: Arithmetic Logic Unit
* `__` Day 25: Sea Cucumber


### 2020

[https://adventofcode.com/2020/][aoc2020]; 29/50 stars (58% complete).

* `**` Day 1: Report Repair
* `**` Day 2: Password Philosophy
* `**` Day 3: Toboggan Trajectory
* `**` Day 4: Passport Processing
* `**` Day 5: Binary Boarding
* `**` Day 6: Custom Customs
* `**` Day 7: Handy Haversacks
* `**` Day 8: Handheld Halting
* `**` Day 9: Encoding Error
* `**` Day 10: Adapter Array
* `*_` Day 11: Seating System
* `**` Day 12: Rain Risk
* `*_` Day 13: Shuttle Search
* `*_` Day 14: Docking Data
* `**` Day 15: Rambunctious Recitation
* `*_` Day 16: Ticket Translation
* `__` Day 17: Conway Cubes
* `__` Day 18: Operation Order
* `__` Day 19: Monster Messages
* `__` Day 20: Jurassic Jigsaw
* `__` Day 21: Allergen Assessment
* `*_` Day 22: Crab Combat
* `__` Day 23: Crab Cups
* `__` Day 24: Lobby Layout
* `__` Day 25: Combo Breaker


### 2019

[https://adventofcode.com/2019/][aoc2019]; 8/50 stars (16% complete).

* `**` Day 1: The Tyranny of the Rocket Equation
* `**` Day 2: 1202 Program Alarm
* `__` Day 3: Crossed Wires
* `**` Day 4: Secure Container
* `__` Day 5: Sunny with a Chance of Asteroids
* `__` Day 6: Universal Orbit Map
* `__` Day 7: Amplification Circuit
* `**` Day 8: Space Image Format
* `__` Day 9: Sensor Boost
* `__` Day 10: Monitoring Station
* `__` Day 11: Space Police
* `__` Day 12: The N-Body Problem
* `__` Day 13: Care Package
* `__` Day 14: Space Stoichiometry
* `__` Day 15: Oxygen System
* `__` Day 16: Flawed Frequency Transmission
* `__` Day 17: Set and Forget
* `__` Day 18: Man-Worlds Interpretation
* `__` Day 19: Tractor Beam
* `__` Day 20: Donut Maze
* `__` Day 21: Springdroid Adventure
* `__` Day 22: Slam Shuffle
* `__` Day 23: Category Six
* `__` Day 24: Planet of Discord
* `__` Day 25: Cryostasis


### 2018

[https://adventofcode.com/2018/][aoc2018]; 10/50 stars (20% complete).

* `**` Day 1: Chronal Calibration
* `**` Day 2: Inventory Management System
* `**` Day 3: No Matter How You Slice It
* `**` Day 4: Repose Record
* `*_` Day 5: Alchemical Reduction
* `__` Day 6: Chronal Coordinates
* `__` Day 7: The Sum Of Its Parts
* `*_` Day 8: Memory Maneuver
* `__` Day 9: Marble Mania
* `__` Day 10: The Stars Align
* `__` Day 11: Chronal Charge
* `__` Day 12: Subterranean Sustainability
* `__` Day 13: Mine Cart Madness
* `__` Day 14: Chocolate Charts
* `__` Day 15: Beverage Bandits
* `__` Day 16: Chronal Classification
* `__` Day 17: Reservoir Research
* `__` Day 18: Settlers of The North Pole
* `__` Day 19: Go With The Flow
* `__` Day 20: A Regular Map
* `__` Day 21: Chronal Conversion
* `__` Day 22: Mode Maze
* `__` Day 23: Experimental Emergency Teleportation
* `__` Day 24: Immune System Simulator 20XX
* `__` Day 25: Four-Dimensional Adventure


### 2017

[https://adventofcode.com/2017/][aoc2017]; 16/50 stars (32% complete).

* `**` Day 1: Inverse Captcha
* `**` Day 2: Corruption Checksum
* `__` Day 3: Spiral Memory
* `**` Day 4: High-Entropy Passphrases
* `*_` Day 5: A Maze of Twisty Trampolines, All Alike
* `**` Day 6: Memory Reallocation
* `*_` Day 7: Recursive Circus
* `__` Day 8: I Heard You Like Registers
* `*_` Day 9: Stream Processing
* `__` Day 10: Knot Hash
* `__` Day 11: Hex Ed
* `__` Day 12: Digital Plumber
* `__` Day 13: Packet Scanners
* `__` Day 14: Disk Defragmentation
* `__` Day 15: Dueling Generators
* `__` Day 16: Permutation Promenade
* `**` Day 17: Spinlock
* `__` Day 18: Duet
* `**` Day 19: A Series of Tubes
* `*_` Day 20: Particle Sawm
* `__` Day 21: Fractal Art
* `__` Day 22: Sporifica Virus
* `__` Day 23: Coprocessor Conflagration
* `__` Day 24: Electromagnetic Moat
* `__` Day 25: The Halting Problem


### 2016

[https://adventofcode.com/2016/][aoc2016]; 0/50 stars (0% complete).

* `__` Day 1: No Time for a Taxicab
* `__` Day 2: Bathroom Security
* `__` Day 3: Squares With Three Sides
* `__` Day 4: Security Through Obscurity
* `__` Day 5: How About a Nice Game of Chess?
* `__` Day 6: Signals and Noise
* `__` Day 7: Internet Protocol Version 7
* `__` Day 8: Two-Factor Authentication
* `__` Day 9: Explosives in Cyberspace
* `__` Day 10: Balance Bots
* `__` Day 11: Radioisotope Thermoelectric Generators
* `__` Day 12: Leonardo's Monorail
* `__` Day 13: A Maze of Twisty Little Cubicles
* `__` Day 14: One-Time Pad
* `__` Day 15: Timing is Everything
* `__` Day 16: Dragon Checksum
* `__` Day 17: Two Steps Forward
* `__` Day 18: Like a Rogue
* `__` Day 19: An Elephant Named Joseph
* `__` Day 20: Firewall Rules
* `__` Day 21: Scrambled Letters and Hash
* `__` Day 22: Grid Computing
* `__` Day 23: Safe Cracking
* `__` Day 24: Air Duct Spelunking
* `__` Day 25: Clock Signal


### 2015

[https://adventofcode.com/2015/][aoc2015]; 14/50 stars (28% complete).

* `**` Day 1: Not Quite Lisp
* `**` Day 2: I Was Told There Would Be No Math
* `**` Day 3: Perfectly Spherical Houses in a Vacuum
* `**` Day 4: The Ideal Stocking Stuffer
* `**` Day 5: Doesn't He Have Intern-Elves For This?
* `**` Day 6: Probably a Fire Hazard
* `__` Day 7: Some Assembly Required
* `**` Day 8: Matchsticks
* `__` Day 9: All in a Single Night
* `__` Day 10: Elves Look, Elves Say
* `__` Day 11: Corporate Policy
* `__` Day 12: JSAbacusFramework.io
* `__` Day 13: Knights of the Dinner Table
* `__` Day 14: Reindeer Olympics
* `__` Day 15: Science for Hungry People
* `__` Day 16: Aunt Sue
* `__` Day 17: No Such Thing as Too Much
* `__` Day 18: Like a GIF For Your Yard
* `__` Day 19: Medicine for Rudolph
* `__` Day 20: Infinite Elves and Infinite Houses
* `__` Day 21: RPG Simulator 20XX
* `__` Day 22: Wizard Simulator 20XX
* `__` Day 23: Opening the Turing Lock
* `__` Day 24: It Hangs in the Balance
* `__` Day 25: Let It Snow


[aoc2015]: http://adventofcode.com/2015/ "Advent of Code - year 2015"
[aoc2016]: http://adventofcode.com/2016/ "Advent of Code - year 2016"
[aoc2017]: http://adventofcode.com/2017/ "Advent of Code - year 2017"
[aoc2018]: http://adventofcode.com/2018/ "Advent of Code - year 2018"
[aoc2019]: http://adventofcode.com/2019/ "Advent of Code - year 2019"
[aoc2020]: http://adventofcode.com/2020/ "Advent of Code - year 2020"
[aoc2021]: http://adventofcode.com/2021/ "Advent of Code - year 2021"
[aoc2022]: http://adventofcode.com/2022/ "Advent of Code - year 2022"
