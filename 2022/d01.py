#! /usr/bin/env python3
""" Day 1: Calorie Counting"""

from aoc_elf import read_input
puzzle_input = read_input("input/01.txt","\n\n")

def part_1():
    max_calories = 0
    for elf in puzzle_input:
        calories = 0
        calories = sum(map(int, elf.split("\n")))
        if calories > max_calories:
            max_calories = calories
    print(max_calories)
    return

def part_2():
    total_calories = []
    for elf in puzzle_input:
        total_calories.append(sum(map(int, elf.split("\n"))))
    print(sum(sorted(total_calories, reverse=True)[:3]))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
