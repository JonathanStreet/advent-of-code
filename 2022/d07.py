#! /usr/bin/env python3
""" Day 7: No Space left on Device """

from aoc_elf import read_input
puzzle_input = read_input("input/07.txt","\n")

def part_1():

    directory_size = {}
    directory_list = []

    for line in puzzle_input:
        command = line.split()
        if command[0] == "$":
            if len(command) == 3:
                if command[2] == "..":
                    directory_list.pop()
                else:
                    directory_list.append(command[2])
                    directory_size[''.join(directory_list)] = 0
        elif command[0] == "dir":
            continue
        else:
            cwd_list = directory_list.copy()
            while len(cwd_list) != 0:
                directory_size[''.join(cwd_list)] += int(command[0])
                cwd_list.pop()

    running_total = 0
    for directory in directory_size:
        if directory_size[directory] <= 100000:
            running_total += directory_size[directory]

    print(running_total)
    return

def part_2():

    directory_size = {}
    directory_list = []

    for line in puzzle_input:
        command = line.split()
        if command[0] == "$":
            if len(command) == 3:
                if command[2] == "..":
                    directory_list.pop()
                else:
                    directory_list.append(command[2])
                    directory_size[''.join(directory_list)] = 0
        elif command[0] == "dir":
            continue
        else:
            cwd_list = directory_list.copy()
            while len(cwd_list) != 0:
                directory_size[''.join(cwd_list)] += int(command[0])
                cwd_list.pop()

    total_disk_space = 70000000
    space_needed =     30000000
    space_used = directory_size['/']
    space_required = space_needed - (total_disk_space - space_used)
    suitable_directory = []
    for directory in directory_size:
        if directory_size[directory] >= space_required:
            suitable_directory.append(directory_size[directory])

    suitable_directory.sort()
    print(suitable_directory[0])
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
