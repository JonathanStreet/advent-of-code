#! /usr/bin/env python3
""" Day 2: Rock Paper Scissors"""

from aoc_elf import read_input
puzzle_input = read_input("input/02.txt","\n")

def part_1():

    running_total = 0
    for entry in puzzle_input:
        total = 0
        score = 0
        p1,p2 = entry.split()

        # A, Y = Rock (1)
        # B, X = Paper (2)
        # C, Z = Scissors (3)
        # Lost = 0
        # Draw = 3
        # Win = 6

        match p1:
            case "A":
                p1_val = 1
            case "B":
                p1_val = 2
            case "C":
                p1_val = 3

        match p2:
            case "X":
                p2_val = 1
            case "Y":
                p2_val = 2
            case "Z":
                p2_val = 3

        # Draw scenario
        if (p1_val == p2_val):
            score = 3
        else:
            match p1_val:
                case 1:
                    # Paper beats rock
                    if p2_val == 2:
                        score = 6
                case 2:
                    # Scissors beats paper
                    if p2_val == 3:
                        score = 6
                case 3:
                    # Rock beats scissors
                    if p2_val == 1:
                        score = 6

        total = p2_val + score
        running_total += total

    print(running_total)
    return

def part_2():

    running_total = 0
    for entry in puzzle_input:
        total = 0
        score = 0
        p1,p2 = entry.split()

        # A, Y = Rock (1)
        # B, X = Paper (2)
        # C, Z = Scissors (3)

        # Y = Need to draw
        # X = Need to lose
        # Z = Need to win

        # Lost = 0
        # Draw = 3
        # Win = 6

        match p1:
            # Paper
            case "A":
                p1_val = 1
            # Rock
            case "B":
                p1_val = 2
            # Scissors
            case "C":
                p1_val = 3

        match p2:
            case "X":
                # lose scenario
                score = 0
            case "Y":
                # draw scenario
                score = 3
            case "Z":
                # win scenario
                score = 6

        # If drawing, I need to play the same value
        if score == 3:
            p2_val = p1_val

        elif score == 6:
            # I need to win
            match p1_val:
                case 1:
                    # Paper beats rock
                    p2_val = 2
                case 2:
                    # Scissors beat paper
                    p2_val = 3
                case 3:
                    # Rock beats Scissors
                    p2_val = 1

        elif score == 0:
            # I need to lose
            match p1_val:
                case 1:
                    # Scissors lose to rock
                    p2_val = 3
                case 2:
                    # Rock loses to paper
                    p2_val = 1
                case 3:
                    # Paper loses to scissors
                    p2_val = 2

        total = p2_val + score
        running_total += total

    print(running_total)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
