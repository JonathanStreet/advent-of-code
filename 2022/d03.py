#! /usr/bin/env python3
""" Day 3: Rucksack Reorganization"""

from aoc_elf import read_input
puzzle_input = read_input("input/03.txt","\n")

def part_1():
    running_total = 0
    for line in puzzle_input:

        # Split each input line into two parts
        first, second = line[:len(line)//2],line[len(line)//2:]

        for ch in first:
            if ch in second:
                # Get the ascii value to use in the calculations
                if ch.islower():
                    running_total += ord(ch) - 96
                else:
                    running_total += ord(ch) - 38
                break

    print(running_total)
    return

def part_2():
    running_total = 0
    i = 0
    j = 3
    while True:
        three_elfs = puzzle_input[i:j]
        if len(three_elfs) == 0:
            break
        one,two,three = three_elfs
        for ch in one:
            if ch in two:
                if ch in three:
                    if ch.islower():
                        running_total += ord(ch) - 96
                    else:
                        running_total += ord(ch) - 38
                    break
        i+=3
        j+=3
    print(running_total)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
