#! /usr/bin/env python3
""" Day 5: Supply Stacks """

from aoc_elf import read_input
from collections import deque

puzzle_input = read_input("input/05.txt","\n\n")
stacks = puzzle_input[0]
instructions = puzzle_input[1]

def parse_stacks(stacks):

    stacks_partial = stacks.split("\n")
    number_of_stacks = len(stacks_partial[-1:][0].split())
    height_of_stacks = len(stacks_partial)-1
    instructions = stacks_partial[:-1]

    stack_list = [""]*number_of_stacks

    # Initialise the stacks
    for i in range(number_of_stacks):
        stack_list[i] = deque([])

    # Populate the stacks
    n = 4
    for i, line in enumerate(instructions):
        for j in range(0,len(line),n):
            character = line[j:j+n].replace('[','').replace(']','').strip()
            stack_number = j//4
            if character != '':
                stack_list[stack_number].appendleft(character)

    return stack_list

def part_1():

    local_stacks = parse_stacks(stacks)

    # Start stack positions
    #print(local_stacks)

    for i in instructions.split("\n"):
        _, val1, _, val2, _, val3 = i.split()
        number_to_move = int(val1)
        from_stack = int(val2) - 1
        to_stack = int(val3) - 1

        # Make the moves
        for j in range(0,int(number_to_move)):
            value = local_stacks[int(from_stack)].pop()
            local_stacks[to_stack].append(value)

    # Final stack positions
    #print(local_stacks)

    final_list = ""
    # Get the top of each stack:
    for i,j in enumerate(local_stacks):
        final_list += local_stacks[i].pop()

    print(final_list)
    
    return

def part_2():

    local_stacks = parse_stacks(stacks)

    for i in instructions.split("\n"):
        _, val1, _, val2, _, val3 = i.split()
        number_to_move = int(val1)
        from_stack = int(val2) - 1
        to_stack = int(val3) - 1

        # Make the moves
        temp_stack = []
        for j in range(0,int(number_to_move)):
            value = local_stacks[int(from_stack)].pop()
            temp_stack.append(value)
        for j in range(0,len(temp_stack)):
            value = temp_stack.pop()
            local_stacks[to_stack].append(value)

    final_list = ""
    # Get the top of each stack:
    for i,j in enumerate(local_stacks):
        final_list += local_stacks[i].pop()

    print(final_list)

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
