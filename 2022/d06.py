#! /usr/bin/env python3
""" Day 6: Tuning Trouble """

from aoc_elf import read_input
puzzle_input = read_input("input/06.txt","\n")[0]

def has_unique_characters(string,n):
    if len(set(string)) == n:
        return True
    else:
        return False

def calculate(n):
    solution = "" 
    for i in range(len(puzzle_input)):
        if has_unique_characters(puzzle_input[i:i+n],n):
            solution = i+n
            break
    return solution

def part_1():
    print(calculate(4))

def part_2():
    print(calculate(14))

print("Part 1:")
part_1()

print("Part 2:")
part_2()
