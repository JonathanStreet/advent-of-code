#! /usr/bin/env python3
""" aoc_elf.py - A helper module for Advent of Code challenges. """

def read_input(input_file, delimiter=None, format=None):
    """ Returns a list of file contents, optional delimiter. """
    with open(input_file) as f:
        contents = f.read()[:-1]

        if format == "str":
            return [i for i in contents.split(delimiter)]
        elif format == "int":
            return [int(i) for i in contents.split(delimiter)]
        else:
            # Check first line of input to see if binary
            is_binary = all(c in '01' for c in contents.split(delimiter)[0])

            if (is_binary):
                return [i for i in contents.split(delimiter)]

            try:
                return [int(i) for i in contents.split(delimiter)]
            except ValueError:
                return [i for i in contents.split(delimiter)]

def multiply_list(item_list):
    """ Multiply all items in a list. """
    total = 1
    for i in range(0, len(item_list)):
        total *= item_list[i]
    return total

