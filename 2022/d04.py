#! /usr/bin/env python3
""" Day 4: Camp Cleanup"""

from aoc_elf import read_input
puzzle_input = read_input("input/04.txt","\n")

def part_1():

    total_pairs = 0
    for section in puzzle_input:
        first, second = section.split(",")

        # Ensure these values are integer, otherwise will do string
        # comparison!
        first_low, first_high = [int(x) for x in first.split("-")]
        second_low, second_high = [int(x) for x in second.split("-")]

        if (first_low >= second_low) and (first_high <= second_high) \
                or (second_low >= first_low) and (second_high <= first_high):
                    total_pairs += 1

    print(total_pairs)
    return

def part_2():

    total_pairs = 0
    for section in puzzle_input:
        first, second = section.split(",")

        # Ensure these values are integer, otherwise will do string
        # comparison!
        first_low, first_high = [int(x) for x in first.split("-")]
        second_low, second_high = [int(x) for x in second.split("-")]

        if (first_low <= second_high) and (first_high >= second_low):
            total_pairs += 1

    print(total_pairs)

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
