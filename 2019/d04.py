#! /usr/bin/env python3
""" Day 4: Secure Container """

# --- Part 1 ---
with open("input/04.txt") as f:
    contents = f.read()[:-1]
    lines = [i for i in contents.split("\n")]

def validateCombination(c,part):
    combo = str(c)
    valid = False

    for i in range(len(combo)-1):
        if (combo[i] > combo[i+1]):
            valid = False
            break

        for digit in combo:
            count = combo.count(digit)
            if (part == 1):
                if count >= 2:
                    valid = True
                    break
            else:
                if count == 2:
                    valid = True
                    break
    return valid

lb, ub = lines[0].split("-")
lb = int(lb)
ub = int(ub)

print("Lower bound:",lb)
print("Upper bound:",ub)

validPasswords = 0
for i in range(lb,ub):
    if (validateCombination(i,1)):
        validPasswords += 1

print("Valid passwords:",validPasswords,"(Part 1)")

# --- Part 2 ---

validPasswords = 0
for i in range(lb,ub):
    if (validateCombination(i,2)):
        validPasswords += 1
print("Valid passwords:",validPasswords,"(Part 2)")
