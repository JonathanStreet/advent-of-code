#! /usr/bin/env python3
""" Day 1: The Tyranny of the Rocket Equation """
with open("input/01.txt") as f:
    contents = f.read()[:-1]
    lines = [int(i) for i in contents.split()]

# --- Part 1 ---
total = 0
fuel  = 0

for mass in lines:
    fuel = int(( mass / 3 ) - 2)
    total += fuel

print(total)


# --- Part 2 ---

def calc_fuel(mass):
    global running_total

    fuel = int(( mass / 3 ) - 2)
    #print("Fuel",fuel,"total",running_total)

    if (fuel > 0):
        running_total += fuel
        calc_fuel(fuel)

running_total_total = 0
for mass in lines:
    running_total = 0
    calc_fuel(mass)
    running_total_total += running_total

print(running_total_total)

