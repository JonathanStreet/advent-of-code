#! /usr/bin/env python3
""" Day 8: Space Image Format """

from textwrap import wrap
import numpy as np

# --- Part 1 ---
with open("input/08.txt") as f:
    contents = f.read()[:-1]
    lines = [i for i in contents.split("\n")]

def findNumberOfDigits(string,digit):
    return string.count(digit)

def findLayerWithLeast0(layers):

    maxZeros = None
    zeros = None
    
    # Loop through the layers
    for l in layers:
        # Return the number of zero's in a layer
        zeros = findNumberOfDigits(l,"0")
        # If a layer has less zeros than previous store it
        if (maxZeros == None) or (zeros < maxZeros):
            maxZeros = zeros
            chosenLayer = l

    return chosenLayer

width = 25
height = 6
charPerLayer = width * height

# Use wrap to split the string into multiple layers
layers = wrap(lines[0],charPerLayer)

# Find the layer with least zeros
chosenLayer = findLayerWithLeast0(layers)

# Checksum is the number of 1-digits mutliplied by number of 2-digits
print("Part 1:",findNumberOfDigits(chosenLayer,"1") * findNumberOfDigits(chosenLayer,"2"))

# --- Part 2 ---

def computeImage(layers, height, width):

    # Get useful variables
    numLayers = len(layers)
    numCharacters = (height*width)
    # Set a pre-defined image
    flattenedImage = list("-" * numCharacters)

    # Loop though every character in a layer
    for ch in range(0,numCharacters):
        # Loop through every layer
        for layer in range(0,numLayers):

            # If we find a 0, pixel is white
            if layers[layer][ch] == "0":
                flattenedImage[ch] = " "
                break;
            # If we find a 1, pixel is black
            if layers[layer][ch] == "1":
                flattenedImage[ch] = "#"
                break;
            # Otherwise pixel is transparent,
            # stay in the loop...

    # Split the string into lists the size of the width of the image
    image = wrap("".join(flattenedImage),width)
    return image

image = computeImage(layers, height, width)

print("Part 2:")
# Print the output in readable format
print('\n'.join(' '.join(str(x) for x in row) for row in image))

