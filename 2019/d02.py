#! /usr/bin/env python3
""" Day 2: 1202 Program Alarm """
with open("input/02.txt") as f:
    contents = f.read()[:-1]
    data = [int(i) for i in contents.split(",")]

# --- Part 1 ---

#program = [1,0,0,0,99]
#program = [2,3,0,3,99]
#program = [1,1,1,4,99,5,6,0,99]
#program = [1,9,10,3,2,3,11,0,99,30,40,50]

#print(data)

def intCodeParser(program, noun, verb):

    program = program[:] # Important that we copy the list!

    # Initialise the program
    address = 0
    program[1] = noun
    program[2] = verb

    while True:
        opcode = program[address]
        if opcode == 99: break

        value1 = program[program[address + 1]]
        value2 = program[program[address + 2]]
        store  = program[address + 3]

        #print("opcode",opcode,"value1",value1,"value2",value2,"store",store)

        if opcode == 1:
            #print("Adding",value1,"+",value2)
            program[store] = value1 + value2
        elif opcode == 2:
            #print("Multiplying",value1,"*",value2)
            program[store] = value1 * value2
        else:
            #print("Something went wrong.")
            break;

        address += 4   

    #print(program)
    return program[0]

print(intCodeParser(data, 12, 2))


# --- Part 2 ---

def calculateNounVerb(solution):
    for noun in range(100):
        for verb in range(100):
            if intCodeParser(data, noun, verb) == solution:
                return (100 * noun) + verb

print(calculateNounVerb(19690720))

