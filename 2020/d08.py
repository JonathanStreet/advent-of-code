#! /usr/bin/env python3
""" Day 8: Handheld Halting """

from aoc_elf import read_input
puzzle_input = read_input("input/08.txt", "\n")

def run_program(instructions):
    """ Run the program. Returns the accumulator & and if it terminates. """

    line_number = 0
    accumulator = 0
    visited = []
    while True:

        # Read input
        operation, argument = instructions[line_number].split()
        argument = int(argument)

        # Create a list of positions in the instruction set that we've already
        # visited.
        visited.append(line_number)

        # If 'No OPeration' move to next position.
        if operation == "nop":
            line_number += 1

        # If 'accumulator' adjust the value.
        elif operation == "acc":
            accumulator += argument
            line_number += 1

        # If 'Jump' move to new function relative to current position.
        elif operation == "jmp":
            line_number += argument

        # If the line has already been visited, the program will loop
        # indefinitely.
        if line_number in visited:
            return accumulator, False

        # Terminates normally if line_number goes past the end of the
        # instructions.
        if line_number >= len(instructions):
            return accumulator, True

def part_1():
    """ Solve part 1 """

    accumulator, _ = run_program(puzzle_input)
    print(accumulator)
    return

def part_2():
    """ Solve part 2 """

    # Loop through all the instructions.
    for i, instruction in enumerate(puzzle_input):

        # Need to copy the value of puzzle_input so that we don't overwrite
        # the original data.
        temp_input = puzzle_input[:]
        operation, argument = instruction.split()

        # If the operation is 'acc' skip the entry.
        if operation == "acc":
            continue

        # If the operation is 'nop' replace with 'jmp'
        elif operation == "nop":
            temp_input[i] = "jmp "+argument

        # If the operation is 'jmp' replace with 'nop'
        elif operation == "jmp":
            temp_input[i] = "nop "+argument

        accumulator, terminated = run_program(temp_input)

        if terminated:
            break

    print(accumulator)
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

