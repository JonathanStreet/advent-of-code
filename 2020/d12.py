#! /usr/bin/env python3
""" Day 12: Rain Risk """

from aoc_elf import read_input
puzzle_input = read_input("input/12.txt")
coords = [0, 0]

def move(facing, units):
    """ Calculate the new co-ordinates """

    if facing == "N":
        coords[0] = coords[0] + int(units)
    elif facing == "E":
        coords[1] = coords[1] + int(units)
    elif facing == "S":
        coords[0] = coords[0] - int(units)
    elif facing == "W":
        coords[1] = coords[1] - int(units)

    return

def part_1():
    """ Solve part 1 """

    # Start facing East.
    facing = "E"

    # Loop over the instructions.
    for instruction in puzzle_input:

        # Parse the instructions
        direction, units = instruction[:1], instruction[1:]

        # Turn clockwise
        if (direction == "R" and units == "90") \
        or (direction == "L" and units == "270"):
            if facing == "N":
                facing = "E"
            elif facing == "E":
                facing = "S"
            elif facing == "S":
                facing = "W"
            elif facing == "W":
                facing = "N"

        # Turn anti-clockwise
        if (direction == "R" and units == "270") \
        or (direction == "L" and units == "90"):
            if facing == "N":
                facing = "W"
            elif facing == "E":
                facing = "N"
            elif facing == "S":
                facing = "E"
            elif facing == "W":
                facing = "S"

        # Turn around
        if direction in ["R", "L"]:
            if units == "180":
                if facing == "N":
                    facing = "S"
                elif facing == "E":
                    facing = "W"
                elif facing == "S":
                    facing = "N"
                elif facing == "W":
                    facing = "E"

        # Move based on the way we're facing.
        if direction == "F":
            move(facing, units)

        # Move based on cardinal directions.
        elif direction in ["N", "E", "S", "W"]:
            move(direction, units)

    print(abs(coords[0]) + abs(coords[1]))

    return

def part_2():
    """ Solve part 2 """

    # Reset the positions
    coords = [0, 0]
    t_waypoint_coords = [0, 0]
    waypoint_coords = [10, 1]

    # Loop over the instructions.
    for instruction in puzzle_input:

        # Parse the instructions
        direction, units = instruction[:1], instruction[1:]

        # Move the waypoint by the given value
        if direction == "N":
            waypoint_coords[1] = waypoint_coords[1] + int(units)
        elif direction == "E":
            waypoint_coords[0] = waypoint_coords[0] + int(units)
        elif direction == "S":
            waypoint_coords[1] = waypoint_coords[1] - int(units)
        elif direction == "W":
            waypoint_coords[0] = waypoint_coords[0] - int(units)

        # Move the ship forward towards the waypoint
        elif direction == "F":
            coords[0] = coords[0] + waypoint_coords[0] * int(units)
            coords[1] = coords[1] + waypoint_coords[1] * int(units)

        # Rotate the waypoint around the ship
        elif direction in ["R", "L"]:

            # Turn clockwise
            if (direction == "R" and units == "90") \
            or (direction == "L" and units == "270"):
                t_waypoint_coords[0] = waypoint_coords[1]
                t_waypoint_coords[1] = waypoint_coords[0] * -1

            # Turn anti-clockwise
            if (direction == "R" and units == "270") \
            or (direction == "L" and units == "90"):
                t_waypoint_coords[0] = waypoint_coords[1] * -1
                t_waypoint_coords[1] = waypoint_coords[0]

            # Turn around
            if (direction in ["R", "L"] and units == "180"):
                t_waypoint_coords[0] = waypoint_coords[0] * -1
                t_waypoint_coords[1] = waypoint_coords[1] * -1

            waypoint_coords[0] = t_waypoint_coords[0]
            waypoint_coords[1] = t_waypoint_coords[1]

    print(abs(coords[0]) + abs(coords[1]))

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

