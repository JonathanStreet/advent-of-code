#! /usr/bin/env python3
""" Day 15: Rambunctions Recitation """

from aoc_elf import read_input
puzzle_input = read_input("input/15.txt", ",")
#puzzle_input = [0, 3, 6]

def part_1():
    """ Solve part 1 """

    numbers_list = puzzle_input.copy()
    rounds = 2020

    # Play the number of turns in the game.
    for _ in range(0, rounds):

        # Get the most recently spoken number
        mrs_number = numbers_list[-1]

        # Check if number occurs in the list.
        if mrs_number in numbers_list[:-1]:

            # Find the most recent occurance of the number.
            most_recent = [i for i, n in enumerate(numbers_list[::-1]) if n == mrs_number][0]

            # Find second-most recent occurance of the number.
            sec_most_recent = [i for i, n in enumerate(numbers_list[::-1]) if n == mrs_number][1]

            new_number = sec_most_recent - most_recent
        else:
            new_number = 0

        # Add the next number to the list
        numbers_list.append(new_number)

    print(numbers_list[rounds-1])
    return

def part_2():
    """ Solve part 2 """

    numbers_list = puzzle_input.copy()
    rounds = 30000000

    # Populate the current status of the game.
    # and store the most recent number.
    game_dict = {}
    for i, n in enumerate(numbers_list):
        game_dict[int(n)] = [i]
        mrs_number = n

    # Play the number of turns in the game.
    for turn in range(len(numbers_list), rounds):

        # If this is the first time the most-recent number has been played the
        # next number should be 0.
        if len(game_dict[mrs_number]) == 1:
            mrs_number = 0
            game_dict[mrs_number] = [game_dict[mrs_number][-1], turn]

        # Otherwise, the number has been played before.
        else:
            diff = game_dict[mrs_number][-1] - game_dict[mrs_number][0]
            if diff in game_dict:
                game_dict[diff] = [game_dict[diff][-1], turn]
            else:
                game_dict[diff] = [turn]
            mrs_number = diff

    print(mrs_number)

    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
