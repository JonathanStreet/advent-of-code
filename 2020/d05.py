#! /usr/bin/env python3
""" Day 5: Binary Boarding """

from aoc_elf import read_input
puzzle_input = read_input("input/05.txt")

def get_seat_list(p_input):
    """ Return the list of seat-ids """
    seat_list = []
    for p in p_input:
        # Replace Front/Back and Left/Right with 0/1.
        ticket = p.replace('F', '0')\
                  .replace('B', '1')\
                  .replace('L', '0')\
                  .replace('R', '1')
        # Split the ticket into row/column.
        row, col = ticket[:7], ticket[-3:]
        # Convert the row/column from binary to decimal to get the seat.
        seat_id = int(row, 2) * 8 + int(col, 2)
        # Add seat to list.
        seat_list.append(seat_id)
    return seat_list

def part_1():
    """ Solve part 1 """
    seat_list = get_seat_list(puzzle_input)
    # "What is the highest seat ID on a boarding pass?"
    print(max(seat_list))
    return

def part_2():
    """ Solve part 2 """
    seat_list = get_seat_list(puzzle_input)
    # Loop through all the seats we know about.
    for seat_id in seat_list:
        # "Seats with IDs +1 and -1 from yours will be in your list"
        if (seat_id + 1 not in seat_list) and (seat_id + 2 in seat_list):
            print(seat_id + 1)
            break
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

