#! /usr/bin/env python3
""" Day 22: Crab Combat """

from aoc_elf import read_input
puzzle_input = read_input("input/22.txt", "\n\n")
#puzzle_input = read_input("input/22-ex.txt", "\n\n")
p1 = list(map(int, puzzle_input[0].split("\n")[1:]))
p2 = list(map(int, puzzle_input[1].split("\n")[1:]))

def play_round():

    no_winner = True
    i = 0
    while no_winner:
        i += 1
        #print("-- Round "+str(i)+" --")
        #print("Player 1's deck:", p1)
        #print("Player 2's deck:", p2)
        #print("Player 1 plays:",p1[0])
        #print("Player 2 plays:",p2[0])

        # Check if player 1 card is greater.
        if p1[0] > p2[0]:
            #print("Player 1 wins the round!")
            # Add player 2 value into player 1 list.
            p1.append(p1[0])
            p1.append(p2[0])
        else:
            #print("Player 2 wins the round!")
            # Add player 1 value into player 2 list.
            p2.append(p2[0])
            p2.append(p1[0])

        # Remove items from start of the list.
        p1.pop(0)
        p2.pop(0)

        if not p1 or not p2:
            no_winner = False

    # Return the winner
    return p1 or p2

def part_1():
    """ Solve part 1 """

    winner = play_round()
    #print(winner)

    counter = 0
    for i, j in enumerate(winner):
        counter += (len(winner) - i) * j

    print(counter)

    return

def part_2():
    """ Solve part 2 """
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

