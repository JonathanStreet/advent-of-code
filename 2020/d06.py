#! /usr/bin/env python3
""" Day 6: Custom Customs """

from aoc_elf import read_input
puzzle_input = read_input("input/06.txt", "\n\n")

def part_1():
    """ Solve part 1 """

    # Merge all people data into single group.
    customs_data = []
    for p in puzzle_input:
        customs_data.append(p.replace("\n", ""))

    # Flatten every individual into a group.
    # Join all individuals, unique the data, and return the length.
    total_count = []
    for group in customs_data:
        total_count.append(len(''.join(set(group))))

    print(sum(total_count))
    return

def part_2():
    """ Solve part 2 """

    customs_data = []
    for p in puzzle_input:
        customs_data.append(p.split())

    total_count = []
    for group in customs_data:
        # Thanks StackOverflow!
        # <https://stackoverflow.com/questions/3852780/python-intersection-of-multiple-lists>
        total_count.append(len(set(group[0]).intersection(*group)))

    print(sum(total_count))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

