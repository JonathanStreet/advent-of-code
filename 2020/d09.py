#! /usr/bin/env python3
""" Day 9: Encoding Error """

from itertools import permutations
from aoc_elf import read_input
puzzle_input = read_input("input/09.txt")

def solve_cipher(counter):
    """ Decypt the eXchange-Masking Addition System (XMAS) """

    preamble = 25

    # Stop if we get outside the boundaries of the input.
    try:
        checksum = puzzle_input[preamble+counter]
    except IndexError:
        return

    # Get a list of the 'preamble' number of items behind the item we're
    # checking.
    temp_input = puzzle_input[:preamble+counter]
    temp_input = temp_input[counter:]

    # Calculate all permutations of the preamble items we're checking.
    check_pass = False
    for a, b in permutations(temp_input, 2):
        # If a match is found, the value passes, don't continue checking.
        if a + b == checksum:
            check_pass = True
            break

    # If the item we're checking hasn't passed the validation return it.
    if not check_pass:
        return checksum
    else:
        return

def part_1():
    """ Solve part 1 """

    # Loop over every line of the input until we find a match.
    for i in range(0, len(puzzle_input)):
        answer = solve_cipher(i)
        if answer != None:
            return answer
    return

def part_2():
    """ Solve part 2 """

    p1_solution = part_1()

    # Loop over every line of the input
    for i in range(0, len(puzzle_input)):

        # Move along to check the start position.
        temp_list = puzzle_input[i:]

        # Loop over the items within the range we're currently checking.
        cumulative_sum = 0
        exit_condition = False
        for j in range(0, len(temp_list)):

            # Increase the cumulative counter
            cumulative_sum += temp_list[j]

            # If the cumulative sum matches the invalid number, we have
            # identified the list.
            if cumulative_sum == p1_solution:
                # "add together the smallest and largest number in this
                # contiguous range"
                print(min(temp_list[:j+1]) + max(temp_list[:j+1]))
                exit_condition = True
                break

        # If we've got the solution, don't continue checking.
        if exit_condition:
            break

    return

print("Part 1:")
print(part_1())

print("Part 2:")
part_2()

