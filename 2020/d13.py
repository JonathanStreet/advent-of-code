#! /usr/bin/env python3
""" Day 13: Shuttle Search """

from aoc_elf import read_input
puzzle_input = read_input("input/13.txt")
#print(puzzle_input)

start_time = puzzle_input[0]
full_bus_list = puzzle_input[1].split(",")

def remove_values_from_list(input_list, remove_item):
    """ Remove values from a list, returns a list without those values. """
    return [value for value in input_list if value != remove_item]

def part_1():
    """ Solve part 1 """

    bus_list = remove_values_from_list(full_bus_list, "x")

    bus_found = False
    time = int(start_time)
    bus = None
    while bus_found != True:
        time += 1
        for bus in bus_list:
            if int(time) % int(bus) == 0:
                bus_found = True
                break

    print(int(bus) * (int(time) - int(start_time)))
    return

def part_2():
    """ Solve part 2 """
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

