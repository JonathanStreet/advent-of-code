#! /usr/bin/env python3
""" Day 1: Report Repair """

from aoc_elf import read_input
puzzle_input = read_input("input/01.txt")

def part_1():
    """ Solve part 1 """
    for i in puzzle_input:
        for j in puzzle_input:
            if i + j == 2020:
                print(str(i*j))
                return

def part_2():
    """ Solve part 2 """
    for i in puzzle_input:
        for j in puzzle_input:
            if i + j < 2020:
                for k in puzzle_input:
                    if i + j + k == 2020:
                        print(str(i*j*k))
                        return

print("Part 1:")
part_1()

print("Part 2:")
part_2()
