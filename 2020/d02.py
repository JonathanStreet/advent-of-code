#! /usr/bin/env python3
""" Day 2: Password Philosophy """

from aoc_elf import read_input
puzzle_input = read_input("input/02.txt", "\n")

def part_1():
    """ Solve part 1 """
    count = 0
    for p in puzzle_input:
        matches = p.split(' ')
        lower, upper = matches[0].split('-', 1)
        char_match = matches[1][:1]
        password = matches[2]

        # Get the number of characters that match
        char_count = password.count(char_match)

        # If within the lower & upper bounds, password is good.
        if int(lower) <= char_count and int(upper) >= char_count:
            count += 1

    print(count)

def part_2():
    """ Solve part 2 """
    count = 0
    for p in puzzle_input:
        matches = p.split(' ')
        pos_1, pos_2 = matches[0].split('-', 1)
        char_match = matches[1][:1]
        password = matches[2]

        pos_1_valid = 0
        pos_2_valid = 0

        # Identify if position 1 is a match
        if password[int(pos_1)-1] == char_match:
            pos_1_valid = 1

        # Identify if position 2 is a match
        if password[int(pos_2)-1] == char_match:
            pos_2_valid = 1

        # Exactly one of these positions must contain the given number.
        if pos_1_valid != pos_2_valid:
            count += 1

    print(count)

print("Part 1:")
part_1()

print("Part 2:")
part_2()

