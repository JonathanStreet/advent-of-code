#! /usr/bin/env python3
""" Day 10: Adapter Array """

from functools import lru_cache
from aoc_elf import read_input
puzzle_input = read_input("input/10.txt")

# Add the first entry.
puzzle_input.append(0)

# Add the final entry (accounts for the built-in joltage adapter).
puzzle_input.append(max(puzzle_input)+3)

# Sort the list.
puzzle_input.sort()

def part_1():
    """ Solve part 1 """

    # Loop over the list and compare the values in pairs.
    differences = []
    for i, val in enumerate(puzzle_input):
        diff = val - puzzle_input[i-1]
        if diff > 0:
            differences.append(diff)

    # Count the number of differences.
    print(differences.count(1) * differences.count(3))

    return

# Bruteforce all the possible paths.
# Using 'lru_cache' to store the existing known paths,
# this means that the process will complete within a i
# reasonable amount of time.
@lru_cache(None)
def calculate_path(number):
    """ Recursively calculate the number of all possible paths. """

    # If we reach the first item in the list, stop and return the path.
    if number == 0:
        return 1
    else:
        total = 0
        # Loop through every valid value of jolts (1-3)
        for i in [1, 2, 3]:
            # If the number is in the list, that's a valid path, find the
            # number of possible path differences down from that one.
            if number-i in puzzle_input:
                total += calculate_path(number-i)
        # Give us the running number of paths.
        return total

def part_2():
    """ Solve part 2 """

    print(calculate_path(max(puzzle_input)))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

