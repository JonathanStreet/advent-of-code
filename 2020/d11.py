#! /usr/bin/env python3
""" Day 11: Seating System """

import numpy as np
from aoc_elf import read_input
puzzle_input = read_input("input/11.txt")

checks = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

def part_1():
    """ Solve part 1 """

    # Set the seat-plan from the input and get the size of the seating area.
    seat_plan = puzzle_input[:]
    width = len(seat_plan[0])
    height = len(seat_plan)

    # Read in the starting position into an array.
    seat_plan = np.array([elem for singleList in puzzle_input for elem in singleList])
    seat_plan = np.reshape(seat_plan, (height, width))

    # Create an empty 2d array for the future seat plan.
    next_seat_plan = np.full((height, width), ".")

    # Loop over the seating moves until we reach equilibrium.
    while True:
        for row_index, row in enumerate(seat_plan):
            for col_index, seat in enumerate(row):

                # We never need to check floor-space.
                if seat != ".":

                    # Add the surrounding seats into a list.
                    surround_seats = []
                    for i, j in checks:
                        drow, dcol = row_index + i, col_index + j
                        # Treat out-of-bounds errors as a floor-space.
                        if drow < 0 or drow >= height:
                            surround_seats.append(".")
                        elif dcol < 0 or dcol >= width:
                            surround_seats.append(".")
                        else:
                            surround_seats.append(seat_plan[drow, dcol])

                    # Count the number of occupied seats.
                    occupied_seats = surround_seats.count("#")

                    # If seat is empty and no occupied seats adjacent occupy it.
                    if seat == "L" and occupied_seats == 0:
                        next_seat_plan[row_index, col_index] = "#"
                    # If seat is occupied and four or more seats are occupied, empty.
                    elif seat == "#" and occupied_seats >= 4:
                        next_seat_plan[row_index, col_index] = "L"

        # If the seating plans match, then we can stop.
        if (seat_plan == next_seat_plan).all():
            break

        # Set seat_plan as the new seating plan.
        seat_plan = next_seat_plan.copy()

    # Count the occupied seats (#).
    print(np.count_nonzero(seat_plan == "#"))
    return

def part_2():
    """ Solve part 2 """
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

