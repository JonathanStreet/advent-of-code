#! /usr/bin/env python3
""" Day 16: Ticket Translation """

import re
from aoc_elf import read_input
puzzle_input = read_input("input/16.txt", "\n\n")
#puzzle_input = read_input("input/16-ex.txt", "\n\n")

ticket_rules = puzzle_input[0].split("\n")
my_ticket = puzzle_input[1].split("\n")[1:]
nearby_tickets = puzzle_input[2].split("\n")[1:]

#print(ticket_rules)
#print(my_ticket)
#print(nearby_tickets)

def parse_rules(rules_list):
    """ Parse the ticket rules """

    parsed_rules = {}
    for r in rules_list:
        # Regex - https://regex101.com/r/vNAYE7/2
        ticket_class, min1, max1, min2, max2 = \
                re.compile(r"(\w.*): (\d+)-(\d+) or (\d+)-(\d+)").findall(r)[0]
        parsed_rules[ticket_class] = [(int(min1), int(max1)), (int(min2), int(max2))]
    return parsed_rules

def validate_ticket_field(rules, ticket):
    """ Validate a field in a ticket based on the rules. """

    # Loop over the rules
    for rule in rules:
        # If the ticket is in the range of rules (min, max) (min max) then
        # it's valid, otherwise it's not!
        if ticket in range(rules[rule][0][0], rules[rule][0][1] + 1) \
        or ticket in range(rules[rule][1][0], rules[rule][1][1] + 1):
            return True
    return False

def part_1():
    """ Solve part 1 """

    rules = parse_rules(ticket_rules)
    scanning_error_rate = 0
    for ticket in nearby_tickets:
        for i in [int(x) for x in ticket.split(",")]:
            if not validate_ticket_field(rules, i):
                scanning_error_rate += i

    print(scanning_error_rate)
    return

def part_2():
    """ Solve part 2 """
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

