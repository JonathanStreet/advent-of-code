#! /usr/bin/env python3
""" Day 3: Toboggan Trajectory """

from aoc_elf import read_input, multiply_list
puzzle_input = read_input("input/03.txt")

def is_tree(char):
    """ Find out if character is a tree. """
    return 1 if char == '#' else 0

def trees_on_slope(slope, right_shift, down_shift):
    """ Find the number of trees on a slope. """
    line_length = len(slope[0])
    total_lines = len(slope)
    tree_counter = 0
    for r in range(0, total_lines // down_shift):
        tree_counter += is_tree(slope[r * down_shift][(r * right_shift) % line_length])
    return tree_counter

def part_1():
    """ Solve part 1 """
    print(trees_on_slope(puzzle_input, 3, 1))
    return

def part_2():
    """ Solve part 2 """
    tree_counts = [trees_on_slope(puzzle_input, 1, 1),
                   trees_on_slope(puzzle_input, 3, 1),
                   trees_on_slope(puzzle_input, 5, 1),
                   trees_on_slope(puzzle_input, 7, 1),
                   trees_on_slope(puzzle_input, 1, 2)]
    print(multiply_list(tree_counts))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

