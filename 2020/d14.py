#! /usr/bin/env python3
""" Day 14: Docking Data """

from aoc_elf import read_input
puzzle_input = read_input("input/14.txt", "\n")

def part_1():
    """ Solve part 1 """

    mem = {}
    for entry in puzzle_input:
        key, value = entry.strip().split(" = ")

        if key == "mask":
            mask = value
        else:
            memory_addr = int(key[4:-1])
            memory_val = int(value)

            # Convert the integer to a 37-padded binary.
            binary_value = format(memory_val, "036b")

            # Decode using the bit-mask.
            # Loop over each character of the mask, if it's not X use that
            # value rather than the existing binary value.
            new_binary_value = ""
            for m, v in zip(mask, binary_value):
                if m == "X":
                    new_binary_value += v
                else:
                    new_binary_value += m

            # Write the new masked value into the memory address.
            mem[memory_addr] = int(new_binary_value, 2)

    # "What is the sum of all values left in memory after it completes?"
    print(sum(mem.values()))
    return

def part_2():
    """ Solve part 2 """
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

