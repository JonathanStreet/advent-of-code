#! /usr/bin/env python3
""" Day 7: Handy Haversacks """

import re
from aoc_elf import read_input
puzzle_input = read_input("input/07.txt", "\n")

def itemise_bags():
    """ Read and store all the bags into an array. """

    # Regex returns the outer bag.
    re_bag = re.compile(r'^([\w ]*) bags contain ')

    # Regex returns the contents of the bag.
    re_contents = re.compile(r'(\d{1,}) ([\w ]*) bag(?:s)?')

    # Create a dictionary to store all our bags in.
    bag_dict = {}
    for p in puzzle_input:

        bag = re_bag.findall(p)[0]
        contents = re_contents.findall(p)

        bag_dict[bag] = {}
        for b in contents:
            bag_dict[bag][b[1]] = int(b[0])

    return bag_dict

def search_for_bag(bag_dict, bag, search_term, bags_found):
    """ Recursively search through the bags """

    # If the bag contains the search term, then interate over it.
    if search_term in bag_dict[bag]:
        if bag not in bags_found:
            bags_found.append(bag)
        for b in bag_dict:
            search_for_bag(bag_dict, b, bag, bags_found)

def count_bags(bag_dict, search_term):
    """ Recursively count the bags """

    # Get the contents of the current bag.
    contents = bag_dict[search_term]

    # If the bag is empty, it contains nothing.
    if len(contents) == 0:
        return 0
    else:
        # If it contains items, iterate over the other bags.
        count = 0
        for bag in contents:
            count += bag_dict[search_term][bag]
            count += bag_dict[search_term][bag] * count_bags(bag_dict, bag)
        return count

def part_1():
    """ Solve part 1 """

    bag_dict = itemise_bags()

    # Search through the dictionary.
    bags_found = []
    for bag in bag_dict:
        search_for_bag(bag_dict, bag, "shiny gold", bags_found)

    print(len(bags_found))
    return

def part_2():
    """ Solve part 2 """

    bag_dict = itemise_bags()
    print(count_bags(bag_dict, "shiny gold"))
    return

print("Part 1:")
part_1()

print("Part 2:")
part_2()

