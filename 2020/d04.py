#! /usr/bin/env python3
""" Day 4: Passport Processing """

import re
from aoc_elf import read_input

puzzle_input = read_input("input/04.txt", "\n\n")
passport_data = []

for p in puzzle_input:
    passport_data.append(p.split())

valid_passports = []

def part_1():
    """ Solve part 1 """

    # Valid passports need all these fields
    valid_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

    for entry in passport_data:

        # If there are less than the minimum number of valid fields then it's
        # an invalid entry.
        if len(entry) < len(valid_fields):
            continue

        # Check if fields match
        valid_field_count = 0
        for v in valid_fields:
            r = re.compile(v+":.*")
            if any(r.match(item) for item in entry):
                valid_field_count += 1

        # If all fields match, then append the valid passport into a  list
        if valid_field_count == len(valid_fields):
            valid_passports.append(entry)

    print(len(valid_passports))
    return valid_passports

def part_2():
    """ Solve part 2 """
    valid_2_passports = []
    for entry in valid_passports:

        valid_field_count = 0
        for f in entry:
            field, value = f.split(":")

            # Birth Year - four digits; at least 1920 and most 2002.
            if field == "byr" \
            and len(value) == 4 \
            and (1920 <= int(value) <= 2002):
                valid_field_count += 1

            # Issue Year - Four digits; 2010-2020.
            elif field == "iyr" \
            and len(value) == 4 \
            and (2010 <= int(value) <= 2020):
                valid_field_count += 1

            # Expiration Year - Four digits; 2020-2030.
            elif field == "eyr" \
            and 2020 <= int(value) <= 2030:
                valid_field_count += 1

            # Height - cm or in; if cm 150-193, if in 59-76.
            elif field == "hgt":
                if str(value[-2:]) == "cm" and (150 <= int(value[:-2]) <= 193):
                    valid_field_count += 1
                elif str(value[-2:]) == "in" and (59 <= int(value[:-2]) <= 76):
                    valid_field_count += 1

            # Hair Colour - must match hex code.
            elif field == "hcl" \
            and re.search(r'^#([0-9a-f]{6})', value):
                valid_field_count += 1

            # Eye Colour - must match criteria
            elif field == "ecl" \
            and value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
                valid_field_count += 1

            # Passport ID - nine-digit number.
            elif field == "pid" \
            and len(value) == 9 and value.isdigit():
                valid_field_count += 1

        # We need a minimum of 7 valid fields.
        if valid_field_count == 7:
            valid_2_passports.append(entry)

    print(len(valid_2_passports))
    return

print("Part 1:")
valid_passports = part_1()

print("Part 2:")
part_2()

