#! /usr/bin/env python3
""" aoc_elf.py - A helper module for Advent of Code challenges. """

def read_input(input_file, delimiter=None):
    """ Returns a list of file contents, optional delimiter. """
    with open(input_file) as f:
        contents = f.read()[:-1]
        try:
            return [int(i) for i in contents.split(delimiter)]
        except ValueError:
            return [i for i in contents.split(delimiter)]

def multiply_list(item_list):
    """ Multiply all items in a list. """
    total = 1
    for i in range(0, len(item_list)):
        total *= item_list[i]
    return total

