#! /usr/bin/env python3
""" Day 3: No matter how you slice it """
# Learning to use regex to get inputs, and numpy

import re
import numpy as np


# --- Part 1 ---
with open("input/03.txt") as f:
    contents = f.read()[:-1]
    lines = contents.split("\n")


# See https://regex101.com/r/zfxea1/3 - Magic!
# 're' = regex module. r'\d+' matches numbers.
claims = map(lambda s: map(int, re.findall(r'\d+',s)), lines)

# Create a numpy 2d-array SIZE*SIZE
SIZE = 1000
fabric = np.zeros((SIZE,SIZE))

# Loop through the inputs, adding the rectangles into the array
for (id, x, y, w, h) in claims:
    fabric[x:x+w,y:y+h] += 1

# Find where the fabric is greater than 1 (overlapping), where returns an
# array, so length of array will provide number of overlapping squares.
print(len(np.where(fabric>1)[0]))


# --- Part 2 ---

# Loop through the existing fabric
for (id, x, y, w, h) in claims:
    #  Where the rectangle is filled with '1' then it's not overlapping.
    if np.all(fabric[x:x+w,y:y+h] == 1):
        print(id)

