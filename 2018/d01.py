#! /usr/bin/env python3
""" Day 1: Chronal Calibration """

# --- Part 1 ---
with open("input/01.txt") as f:
    contents = f.read()[:-1]
    freq_change = [int(i) for i in contents.split()]

    print(sum(freq_change))


# --- Part 2 ---
from itertools import cycle
used_frequencies = set([0])
sum = 0

for f in cycle(freq_change):
    sum += f
    if sum in used_frequencies:
        break;
    used_frequencies.add(sum)

print(sum)

