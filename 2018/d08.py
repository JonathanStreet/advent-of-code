#! /usr/bin/env python3
""" Day 8: Memory Maneuver """

with open("input/08.txt") as f:
    contents = f.read()[:-1]
    list = [int(x) for x in contents.split()]

def parser(node):
    num_children, num_metadata = node[:2]
    metadata = node[2:]
    running_total = 0

    # print("Number of Children {C}, Number of Metas {M}".format(C=num_children,M=num_metadata))
    # print((metadata))

    for i in range(num_children):
        total, metadata = parser(metadata)
        running_total += total

    running_total += sum(metadata[:num_metadata])

    header = (num_children,num_metadata)
    node = (running_total, metadata[num_metadata:])
    
    return node

total, metadata = parser(list)
print("Part 1:",total)

