#! /usr/bin/env python3
""" Day 2: Inventory Management System """

# --- Part 1 ---
with open("input/02.txt") as f:
    contents = f.read()[:-1]
    lines = contents.split("\n")

twos = 0
threes = 0

for s in lines:

    usedtwo = False
    usedthree = False

    unique_chars = set(s)

    for uc in unique_chars:
        if (s.count(uc) == 2):
            if not usedtwo: 
                twos += 1;
                usedtwo = True;
        elif (s.count(uc) == 3):
            if not usedthree:
                threes += 1;
                usedthree = True;

checksum = twos * threes
print(checksum)


# --- Part 2 ---

# Hamming distance where length 1
def match(s1,s2):
    return sum(el1 != el2 for el1, el2 in zip(s1,s2)) == 1

# Find the position of the first difference
def diff(s1,s2):
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            return i

for a in lines:
    for b in lines:
        if match(a,b):
            #print("A: {a}\nB: {b}".format(a=a,b=b))
            position = diff(a,b)
            output = a[:int(position)] + a[int(position)+1:]

print(output)

